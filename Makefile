# blink.nim compiled to app.vxp
PROJECT_DIR = examples
PROJECT_NAME = blink

NIM_STDLIB = /usr/lib/nim
DEBUG_LEVEL = 2

SKETCH = $(PROJECT_DIR)/$(PROJECT_NAME)/$(PROJECT_NAME).ino
UPLOAD_PORT = /dev/ttyUSB0
BOARD = esp210
BUILD_DIR = $(PROJECT_DIR)/$(PROJECT_NAME)/build

# We need to do this in two runs since we do not know
# how many .cpp files we will produce in the first rule.
.PHONY: make

build: make
	make -f makeEspArduino/makeEspArduino.mk SKETCH=$(SKETCH) BOARD=$(BOARD) BUILD_DIR=$(BUILD_DIR)
flash: make
	make -f makeEspArduino/makeEspArduino.mk SKETCH=$(SKETCH) UPLOAD_PORT=$(UPLOAD_PORT) BOARD=$(BOARD) BUILD_DIR=$(BUILD_DIR) flash

# Run Nim compilation only step, see nim.cfg for settings
make: prepare
	nim cpp --verbosity:$(DEBUG_LEVEL) -d:release -c $(PROJECT_DIR)/$(PROJECT_NAME).nim 
	
	mkdir $(PROJECT_DIR)/$(PROJECT_NAME)/
	cp $(PROJECT_DIR)/nimcache/*.cpp $(PROJECT_DIR)/$(PROJECT_NAME)/
	
	sed -i "s/throw this;/ /g" examples/blink/*.cpp
	cp $(NIM_STDLIB)/nimbase.h $(PROJECT_DIR)/$(PROJECT_NAME)/nimbase.h
	
	mv $(PROJECT_DIR)/$(PROJECT_NAME)/$(PROJECT_NAME).cpp $(PROJECT_DIR)/$(PROJECT_NAME)/$(PROJECT_NAME).ino


prepare: 
	cp panicoverride.nim $(PROJECT_DIR)/panicoverride.nim
