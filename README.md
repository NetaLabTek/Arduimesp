# Ardunimesp
This is not only a wrapper of the Arduino framework for the esp8266 using the <a href="http://nim-lang.org">programming language Nim</a>, but also a tool for make and convert the nim code in to a Arduino project.

This project basically consists of the wrappers and two Makefiles, one that prepare and make the nim code in to an Arduino project and another one, the [makeEspArduino](https://github.com/plerup/makeEspArduino) that compile and flash the Arduino project.
Based on: https://github.com/gokr/ardunimo
## Get Ardunimo
1. Clone this repo and the submodules to your machine, **only tested on 64 bit Arch linux**. The Makefile presumes Nim standard library is in `/usr/lib/nim`.

## Building blink.nim
In the repository there is a single trivial application - `blink.nim`, you probably recognize it. To build it and flash:

1. Run `make build`. If all is well it produces `examples/blink/build/blink.bin`.

## Building some other
```
make <make|build|flash> PROJECT_NAME=<directory> PROJECT_NAME=<name of the main witout .nim>
```

## NOTES:

* The wrapper is not complete (I just did what I needed) and in many places probably downright silly.
* We compile via Nim's C++ backend since Arduino libraries are C++ based.
* The `ardunimo.nim` is a hack I ended up doing so that setup/loop gets resolved properly. PRs appreciated.
* The wrapper/src directory contains the SDK sources *modified* by c2nim and manually, these are not used when compiling.

